const express = require("express");
const morgan = require("morgan");
const setupPrisma = require("./config/db");
const userRoutes = require("./routes/userRoutes");


const app = express();

app.use(express.json());
app.use(morgan('dev'));

app.use("/", userRoutes);

const PORT = 3001;
setupPrisma();

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
