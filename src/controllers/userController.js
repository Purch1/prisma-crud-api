// const prisma = require('../utils/prisma');
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

const userController = {
    createUser: async (req, res) => {
      try {
        const newUser = await prisma.user.create({ data: req.body });
        res.json(newUser);
      } catch (error) {
        console.error("Error creating user:", error);
        res.status(500).json({ error: "Failed to create user." });
      }
    },
    
    getAllUsers: async (req, res) => {
        try {
            const allUsers = await prisma.user.findMany();
            res.json(allUsers);
        } catch (error) {
            console.error("Error retrieving users:", error);
            res.status(500).json({ error: "Failed to retrieve users." });
        }
    },

    getUserById: async (req, res) => {
        try {
          const { id } = req.params;
          const user = await prisma.user.findUnique({ where: { id: parseInt(id) } });
      
          if (!user) {
            return res.status(404).json({ error: 'User not found.' });
          }
      
          res.json(user);
        } catch (error) {
          console.error('Error retrieving user:', error);
          res.status(500).json({ error: 'Failed to retrieve user.' });
        }
      },
};


module.exports = userController;
